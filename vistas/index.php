<?php

include "paginas/modulos/headerContenido.php";



#ISSET: isset() Determina si una variable está definida y no es NULL

if (isset($_GET["pagina"])) {

    if (
        $_GET["pagina"] == "unete" ||
        $_GET["pagina"] == "mision-vision" ||
        $_GET["pagina"] == "historia" ||
        $_GET["pagina"] == "proyectos" ||
        $_GET["pagina"] == "voluntarios" ||
        $_GET["pagina"] == "eventos" ||
        $_GET["pagina"] == "manada" ||
        $_GET["pagina"] == "tropa" ||
        $_GET["pagina"] == "comunidad" ||
        $_GET["pagina"] == "clan" ||
        $_GET["pagina"] == "promesa-y-ley" ||
        $_GET["pagina"] == "oraciones-scout" ||
        $_GET["pagina"] == "cuadro-de-honor" ||
        $_GET["pagina"] == "cuotas" ||
        $_GET["pagina"] == "ordenamientos"
    ) {

        include "paginas/" . $_GET["pagina"] . ".php";
    } else {

        include "paginas/error404.php";
    }
} else {

    include "paginas/error404.php";
}


include "paginas/modulos/footer.php";