<div class="w3-container" style="padding: 0px 15%;">
  <h1 style="text-align: justify;">ORACIONES de las secciones</h1>
  <div class="w3-row w3-center">
    <div class="w3-container w3-half">
      <h3>Manada de Lobatos</h3>
      <img src="img/logosInstitucionales/Insignia-Manada-Scouts-de-México.png" alt="logo_manada" style="width: 40%;">
      <p style="padding: 0 55px;">Dulce y buen señor mío: enséñame a ser humilde y bondados@, a imitar tu ejemplo, a amarte con todo mi corazón, y a seguir el camino, que me ha de llevar junto a ti. Así sea.</p>
    </div>
    <div class="w3-container w3-half">
      <h3>Tropa Scout</h3>
      <img src="img/logosInstitucionales/Insignia-Tropa-Scouts-de-México.png" alt="logo_tropa" style="width: 40%;">
      <p style="padding: 0 55px;">Señor: enséñame a ser generos@, a servirte como lo mereces, a dar sin medida, a combatir sin miedo a que me hieran, a trabajar sin descanso, y a no buscar más recompensa, que saber que hago tu voluntad. Así sea.</p>
    </div>
    <div class="w3-container w3-half">
      <h3>Comunidad de Caminantes</h3>
      <img src="img/logosInstitucionales/Insignia-Caminantes-Scouts-de-México.png" alt="logo_comunidad" style="width: 40%;">
      <p style="padding: 0 55px;">Señor: ayúdame a encontrar la fortaleza del viejo roble, para que ningún triunfo me envanezca; la alegría de la naturaleza, para que ninguna soledad me abata; la libertad del ave, para elegir mi camino, y la voluntad del caminante, para seguir
        siempre adelante y servir. Así sea.</p>
    </div>
    <div class="w3-container w3-half">
      <h3>Clan de Rovers</h3>
      <img src="img/logosInstitucionales/Insignia-Rover-Scouts-de-México.png" alt="logo_clan" style="width: 40%;">
      <p style="padding: 0 55px;">Dame señor: Un corazón vigilante, que ningún pensamiento vano me aleje de ti. Un corazón noble, que ningún afecto indigno rebaje, Un corazón recto, que ninguna maldad desvíe. Un corazón fuerte, que ninguna pasión esclavice. Un corazón generoso,
        para servir. Así sea.</p>
    </div>
    <div class="w3-container">
      <h3>Scouters y Dirigentes</h3>
      <img src="img/people.png" alt="logo_clan" style="width: 20%;">
      <p style="padding: 0 25%;">Señor, tú que me diste el deber de dirigir, dame ahora la alegría para trabajar, seguridad para disponer y humildad para servir. Convierte en fortaleza mi debilidad, en madurez mi inexperiencia, en habilidad mi torpeza y en serenidad mi impaciencia.
        Quiero con tu ayuda generosa realizar la misión que me has enconmendado. Así sea.</p>
    </div>
  </div>
  <h1 style="text-align: justify;">otras oraciones</h1>
  <div class="w3-row w3-center">
    <div class="w3-container w3-half">
      <h3>Alimentos</h3>
      <img src="img/food.png" alt="logo_clan" style="width: 35%;">
      <p style="padding: 0 55px;">Señor: Tú que das el agua a los campos, la tierra a las plantas, el fruto a los hombres, danos bendito este alimento, para que vuelto fortaleza, podamos servirte mejor. Así sea.</p>
    </div>
    <div class="w3-container w3-half">
      <h3>Fogata</h3>
      <img src="img/nature.png" alt="logo_clan" style="width: 35%;">
      <p style="padding: 0 55px;">Que las llamas se levanten hasta el cielo y con ellas el corazón de los mortales, que el crepitar de sus ardientes brasas llenen al mundo de amor, luz y alegría; y que el Señor bendiga en esta noche a quienes estamos reunidos en torno a ella.
        Así sea.</p>
    </div>
    <div class="w3-container">
      <h3>Padres de familia</h3>
      <img src="img/parent.png" alt="logo_clan" style="width: 20%;">
      <p style="padding: 0 25%;">Señor: tú que nos diste el don de la paternidad, ayúdanos a corregir a nuestros hijos y a guiarlos por el camino correcto de sus inquietudes, danos paciencia para corregir sus desvíos y cordura para respetar sus personas, para tener la satisfacción
        de hacer de ellos unos hombres a imagen y semejanza tuya. Así sea.</p>
    </div>
  </div>
</div>