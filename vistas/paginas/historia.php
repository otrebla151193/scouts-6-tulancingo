<div class="w3-container" style="padding: 0px 15%;">
    <h1 style="text-align: justify;">HISTORIA</h1>

    <h4>FOTO DEL MENUMENTO</h4>

    <p class="w3-justify">El 14 de Octubre de 1978 el grupo 6 comenzó con una manada de 3 seisenas, la tropa con 12 miembros y sin clan.
        Ricardo fue jefe del grupo por 2 años y luego se fue como comisionado dedistrito. El grupo 1 tenía como jefe a Oscar Mendoza Oviedo, el
        distrito de Tulancingo era el 3, Tula el 2 y Real del monte el 1.</p>

    <p class="w3-justify">
        En noviembre del 79 Ricardo se había casado y le pidió a Jesús Rodríguez que se hiciera cargo del distrito.
        El grupo se quedó con el Ingeniero Carlos Germán Alarcón. Siendo Ricardo comisionado se formó el clan del grupo con 16 jóvenes,
        el jefe del clan era Antonio Soto, y algunos de los integrantes: Miguel Martínez Hernández, Victor Peña, Cresencio Hernández y Javier Rivera.También se reforzó la manada nombrando Akela a
        Felipe Ramírez que era de clan. Blanca Ugalde era la subjefa de manada.
    </p>

    <img src="img/historia/histoia1.png" style="width: 100%;" alt="">

    <p class="w3-justify">A nivel nacional se toma la decisión de incluir a las mujeres, por lo que se crearon las unidades femeninas, razón por la cual se separaronn muchos grupos a los que no les
        gustó la idea. A Ricardo lo nombraron Presidente de provincia y comenzó a trabajar apoyándose en el grupo 6, Blanca Ugalde fue comisionada de programa.
    </p>

    <p class="w3-justify"> Las gacelas se reunían en el Jardín La Floresta, entre las primeras guías de la tropa femenina:
        Margarita Alarcón.
    </p>

    <p class="w3-justify">En el 81 Ricardo terminó su insignia de madera, El grupo obtuvo varios premios como el segundo lguar en actividades acuaticas en Veracruz, en Honey ganaron el 1, 2 y 3 lugar y ganaron el primer lugar en el campamento nacional
        de manadas en el 80, y el campeonato nacional de patrullas en el Chico.
    </p>

    <p>Jefes del grupo 6:</p>
    <ul>
        <li>1978: Ricardo Montiel +</li>
        <li>1979: Carlos Germán Alarcón</li>
        <li>1980: Arturo Macias +</li>
        <li>1987: María del Carmen Hernández Pastrana</li>
        <li>1989: Crisoforo Contreras</li>
        <li>1993: Antonio Becerril Barcenas</li>
        <li>1996: José Luis Ortega Pineda</li>
        <li>1999 María Rosa Lugo Vargas</li>
        <li>2000 Alfonso Lasses Carbajal</li>
        <li>2002 Miguel Martínez</li>
        <li>2005 Eduardo Rios Melendez</li>
        <li>2010: Hugo Ramírez + </li>
        <li>2011: Victor Hugo Mendez</li>
        <li>2012: Verónica González</li>
        <li>2013: Alfonso Eduardo Barrios Vargas + </li>
        <li>2014: Juan Carlos López Terrazas</li>
        <li>2015: Alfonso Eduardo Barrios Vargas + </li>
        <li>2016 - Actual: Arturo Negrete Medellín</li>
    </ul>
</div>