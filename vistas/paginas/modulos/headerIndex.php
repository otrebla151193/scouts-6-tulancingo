<!DOCTYPE html>
<html>
<title>Grupo Scout 6 Tulancingo "Quetzalcóatl"</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="vistas/css/scoutsStyleIndex.css">
<link rel="stylesheet" href="vistas/css/materialize.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="icon" type="image/png" href="vistas/img/logosGrupo/logo_redes_sociales.png" />
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">
<meta name="description" content="Página web del grupo scout 6 Tulancingo de la ASMC.">
<style>
  /* Create a Parallax Effect */

  .bgimg-1,
  .bgimg-2,
  .bgimg-3 {
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }

  /* First image (Logo. Full height) */

  .bgimg-1 {
    background-image: url('vistas/img/header/20171007_133251.jpg');
  }

  /* Second image (Portfolio) */

  .w3-wide {
    letter-spacing: 10px;
  }

  .w3-hover-opacity {
    cursor: pointer;
  }

  /* Turn off parallax scrolling for tablets and phones */

  @media only screen and (max-device-width: 3000px) {

    .bgimg-1,
    .bgimg-2,
    .bgimg-3 {
      background-attachment: scroll;
      min-height: 100vh;
      height: 100vh;
    }
  }
</style>

<body>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=1748763862018087&autoLogAppEvents=1"></script>
  <!-- Navbar (sit on top) -->
  <div class="w3-top w3-white topnav">
    <div class="w3-bar w3-card">
      <div class="w3-left">
        <a href="#" class="topnavbrand"><img src="vistas/img/logosGrupo/EscudoGrupo.png" class="navBrandImg" alt=""> Grupo 6 Tulancingo </a>
      </div>
      <div class="w3-right">
        <a class="w3-bar-item w3-button w3-padding-large w3-hide-medium w3-hide-large w3-right" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
        <a href="vistas/index.php?pagina=unete" class="navBarItem">ÚNETE</a>
        <div class="dropdown">
          <button class="dropbtn" title="More">NOSOTROS<i class="fa fa-caret-down"></i></button>
          <div class="dropdown-content w3-bar-block w3-card-4">
            <a class="dropDownContentItem" href="vistas/index.php?pagina=mision-vision">Misión y Visión</a>
            <a class="dropDownContentItem" href="vistas/index.php?pagina=historia">Historia</a>
            <a class="dropDownContentItem" href="vistas/index.php?pagina=proyectos">Proyectos</a>
            <a class="dropDownContentItem" href="vistas/index.php?pagina=voluntarios">Voluntarios</a>
          </div>
        </div>
        <a href="vistas/index.php?pagina=eventos" class="navBarItem">EVENTOS</a>
        <div class="dropdown">
          <button class="dropbtn" title="More">SECCIONES <i class="fa fa-caret-down"></i></button>
          <div class="dropdown-content w3-bar-block w3-card-4">
            <a class="mlHover" href="vistas/index.php?pagina=secciones/manada"><img src="img/logosInstitucionales/Insignia-Manada-Scouts-de-México.png" style="max-height: 50px;" alt="">Manada de Lobatos</a>
            <a class="tsHover" href="vistas/index.php?pagina=secciones/tropa"><img src="img/logosInstitucionales/Insignia-Tropa-Scouts-de-México.png" style="max-height: 50px;" alt="">Tropa de Scouts</a>
            <a class="ccHover" href="vistas/index.php?pagina=secciones/comunidad"><img src="img/logosInstitucionales/Insignia-Caminantes-Scouts-de-México.png" style="max-height: 50px;" alt="">Comunidad de Caminantes</a>
            <a class="crHover" href="vistas/index.php?pagina=secciones/clan"><img src="img/logosInstitucionales/Insignia-Rover-Scouts-de-México.png" style="max-height: 50px;" alt="">Clan de Rovers</a>
          </div>
        </div>
        <div class="dropdown">
          <button class="dropbtn" title="More">SOY SCOUT! <i class="fa fa-caret-down"></i></button>
          <div class="dropdown-content w3-bar-block w3-card-4">
            <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/promesa-y-ley">Promesa y Ley</a>
            <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/oraciones-scouts">Oraciones Scouts</a>
            <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/cuadro-de-honor">Cuadro de Honor</a>
            <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/ordenamientos">Ordenamientos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Navbar on small screens (remove the onclick attribute if you want the navbar to always show on top of the content when clicking on the links) -->
  <div id="navDemo" class="w3-hide w3-hide-large w3-hide-medium w3-top topnav responsive" style="margin-top:46px">
    <a href="vistas/index.php?pagina=unete" class="navBarItem">ÚNETE</a>
    <div class="dropdown">
      <button class="dropbtn" title="More">NOSOTROS<i class="fa fa-caret-down"></i></button>
      <div class="dropdown-content w3-bar-block w3-card-4">
        <a class="dropDownContentItem" href="vistas/index.php?pagina=mision-vision">Misión y Visión</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=historia">Historia</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=proyectos">Proyectos</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=voluntarios">Voluntarios</a>
      </div>
    </div>
    <a href="vistas/index.php?pagina=eventos" class="navBarItem">EVENTOS</a>
    <div class="dropdown">
      <button class="dropbtn" title="More">SECCIONES <i class="fa fa-caret-down"></i></button>
      <div class="dropdown-content w3-bar-block w3-card-4">
        <a class="mlHover" href="vistas/index.php?pagina=secciones/manada"><img src="img/logosInstitucionales/Insignia-Manada-Scouts-de-México.png" style="max-height: 50px;" alt="">Manada de Lobatos</a>
        <a class="tsHover" href="vistas/index.php?pagina=secciones/tropa"><img src="img/logosInstitucionales/Insignia-Tropa-Scouts-de-México.png" style="max-height: 50px;" alt="">Tropa de Scouts</a>
        <a class="ccHover" href="vistas/index.php?pagina=secciones/comunidad"><img src="img/logosInstitucionales/Insignia-Caminantes-Scouts-de-México.png" style="max-height: 50px;" alt="">Comunidad de Caminantes</a>
        <a class="crHover" href="vistas/index.php?pagina=secciones/clan"><img src="img/logosInstitucionales/Insignia-Rover-Scouts-de-México.png" style="max-height: 50px;" alt="">Clan de Rovers</a>
      </div>
    </div>
    <div class="dropdown">
      <button class="dropbtn" title="More">SOY SCOUT! <i class="fa fa-caret-down"></i></button>
      <div class="dropdown-content w3-bar-block w3-card-4">
        <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/promesa-y-ley">Promesa y Ley</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/oraciones-scouts">Oraciones Scouts</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/cuadro-de-honor">Cuadro de Honor</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/cuotas">Cuotas</a>
        <a class="dropDownContentItem" href="vistas/index.php?pagina=soy-scout/ordenamientos">Ordenamientos</a>
      </div>
    </div>
  </div>



  <!-- First Parallax Image with Logo Text -->
  <div class="bgimg-1 w3-display-container w3-opacity-min" id="home">
    <div class="w3-display-middle" style="white-space:nowrap;">
      <p class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">Construyamos juntos</p>
      <p class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">un mundo mejors.</p>
      <p class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">#scouts6tulancingo</p>
    </div>
  </div>

  <script>
    // Used to toggle the menu on small screens when clicking on the menu button
    function myFunction() {
      var x = document.getElementById("navDemo");
      if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
      } else {
        x.className = x.className.replace(" w3-show", "");
      }
    }
  </script>