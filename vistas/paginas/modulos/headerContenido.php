<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/scoutsStyle.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Architects Daughter' rel='stylesheet'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="icon" type="image/png" href="img/logosGrupo/logo_redes_sociales.png" />
  <title>Grupo Scout 6 Tulancingo "Quetzalcóatl"</title>
</head>

<body>
  <div class="icon-bar">
    <a href="https://www.facebook.com/Grupo6Quetzalcoatl" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/scouts6tulancingo/?hl=es-la" target="_blank" class="youtube"><i class="fa fa-instagram"></i></a>
  </div>

  <div class="topnav" id="myTopnav">
    <div class="w3-left">
      <a href="/" class="topnavbrand"><img src="img/logosGrupo/EscudoGrupo.png" class="navBrandImg" alt="">
        Grupo 6 Tulancingo </a>
    </div>
    <div class="w3-right">
      <a href="index.php?pagina=unete" class="navBarItem">Únete</a>
      <div class="dropdown">
        <button class="dropbtn">Nosotros
          <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a class="dropDownContentItem" href="index.php?pagina=mision-vision">Misión y Visión</a>
          <a class="dropDownContentItem" href="index.php?pagina=historia">Historia</a>
          <a class="dropDownContentItem" href="index.php?pagina=proyectos">Proyectos</a>
          <a class="dropDownContentItem" href="index.php?pagina=voluntarios">Voluntarios</a>
        </div>
      </div>
      <a href="index.php?pagina=eventos" class="navBarItem">Eventos</a>
      <div class="dropdown">
        <button class="dropbtn">Secciones
          <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a class="mlHover" href="index.php?pagina=manada"><img src="img/logosInstitucionales/Insignia-Manada-Scouts-de-México.png" style="max-height: 50px;" alt="">Manada de Lobatos</a>
          <a class="tsHover" href="index.php?pagina=tropa"><img src="img/logosInstitucionales/Insignia-Tropa-Scouts-de-México.png" style="max-height: 50px;" alt="">Tropa
            de Scouts</a>
          <a class="ccHover" href="index.php?pagina=comunidad"><img src="img/logosInstitucionales/Insignia-Caminantes-Scouts-de-México.png" style="max-height: 50px;" alt="">Comunidad de Caminantes</a>
          <a class="crHover" href="index.php?pagina=clan"><img src="img/logosInstitucionales/Insignia-Rover-Scouts-de-México.png" style="max-height: 50px;" alt="">Clan
            de Rovers</a>
        </div>
      </div>
      <div class="dropdown">
        <button class="dropbtn">Soy Scout!<i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a class="dropDownContentItem" href="index.php?pagina=promesa-y-ley">Promesa y Ley</a>
          <a class="dropDownContentItem" href="index.php?pagina=oraciones-scout">Oraciones Scouts</a>
          <a class="dropDownContentItem" href="index.php?pagina=cuadro-de-honor">Cuadro de Honor</a>
          <a class="dropDownContentItem" href="index.php?pagina=ordenamientos">Ordenamientos</a>
        </div>
      </div>
      <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
    </div>
  </div>

  <script>
    function myFunction() {
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
  </script>