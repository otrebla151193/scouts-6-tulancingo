<div class="w3-container" style="padding: 0px 15%;">
    <H1>VOLUNTARIOS</H1>
    <P><i>&quot;"El papel de un dirigente scout, no es el de un maestro, ni el de un padre o madre, ni el de un
            sacerdote, ni de un entrenador deportivo, ni mucho menos de un instructor militar; simplemente, el de un
            hermano mayor que considera las cosas desde el punto de vista de los jóvenes, los guía por el buen sendero y
            les transmite entusiasmo."&quot;</i></P>
    <span style="float: right;">
        <p> Baden Powell. Guía para el jefe de tropa, 1919</p>
    </span>
    <br>
    <br>
    <div>
        <H2 class="w3-center">Jefatura de grupo</H2>
        <br>
        <div class="w3-row">
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>JEFE DE GRUPO</h4>
                        <br>
                        <img src="img/voluntarios/Jefe Arturo.jpg" alt="Avatar" style="width:80%">
                        <P>ARTURO NEGRETE MEDELLÍN</P>
                        <div class="w3-section">
                            <button onclick="myFunctionModal('Demo1')" class="w3-button backOficial">Contacto</button>
                            <div id="Demo1" class="w3-hide w3-container w3-justify">
                                <p>Teléfono: 777777777</p>
                                <p>Correo: hgo6.tulancingo@scouts.org.mx</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>SUBJEFA DE ADMINISTRACIÓN</h4>
                        <br>
                        <img src="img/voluntarios/Miretito.jpg" alt="Avatar" style="width:80%">
                        <P>MIREYA MENDEZ ORTIZ</P>
                        <div class="w3-section">
                            <button onclick="myFunctionModal('Demo2')" class="w3-button backOficial">Contacto</button>
                            <div id="Demo2" class="w3-hide w3-container w3-justify">
                                <p>Teléfono: 777777777</p>
                                <p>Correo: hgo6.tulancingo@scouts.org.mx</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>SUBJEFA DE MÉTODOS EDUCATIVOS</h4>
                        <img src="img/voluntarios/Mariel.jpg" alt="Avatar" style="width:80%">
                        <P>MARIEL FLORES DÍAZ</P>
                        <div class="w3-section">
                            <button onclick="myFunctionModal('Demo3')" class="w3-button backOficial">Contacto</button>
                            <div id="Demo3" class="w3-hide w3-container w3-justify">
                                <p>Teléfono: 7712208839</p>
                                <p>Correo: hgo6.tulancingo@scouts.org.mx</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <div>
        <H2 class="w3-center">Manada de Lobatos</H2>
        <br>
        <div class="w3-row">
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>AKELA (JEFA DE SECCIÓN)</h4>
                        <br>
                        <img src="img/voluntarios/Gaby.jpg" alt="Avatar" style="width:80%">
                        <P>GABRIELA OLVERA BADILLO</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                            <div id="Demo1" class="w3-hide w3-container w3-justify">
                                <p>Teléfono: 777777777</p>
                                <p>Correo: hgo6.tulancingo@scouts.org.mx</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>RAKSHA (SUB JEFA DE SECCIÓN)</h4>
                        <br>
                        <img src="img/voluntarios/Rocio.jpg" alt="Avatar" style="width:80%">
                        <P>ROCIO HERNANDEZ RAMOS</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>(SUB JEFA DE SECCIÓN)</h4>
                        <br>
                        <img src="img/voluntarios/Gabina.jpg" alt="Avatar" style="width:80%">
                        <P>MARIA GABINA CLAUDIA RAMOS FLORES</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <div>
        <H2 class="w3-center">Tropa de Scouts</H2>
        <br>
        <div class="w3-row">
            <div class="w3-container" style="width: 16.66%; float: left;">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>JEFA DE SECCIÓN</h4>
                        <br>
                        <img src="img/voluntarios/avatar.png" alt="Avatar" style="width:80%">
                        <P>YOLANDA MANJARREZ ISLAS</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>SCOUTER DE APOYO</h4>
                        <br>
                        <img src="img/voluntarios/perfil_gmail.png" alt="Avatar" style="width:80%">
                        <P>ALBERTO APARICIO LAZCANO</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <div>
        <H2 class="w3-center">Comunidad de Caminantes</H2>
        <br>
        <div class="w3-row">
            <div class="w3-container" style="width: 16.66%; float: left;">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>JEFE DE SECCIÓN</h4>
                        <br>
                        <img src="img/voluntarios/IMG_20181117_180437 (2).jpg" alt="Avatar" style="width:80%">
                        <P>CARLOS ALEJANDRO OBREGÓN LAZCANO</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>SCOUTER DE APOYO</h4>
                        <br>
                        <img src="img/voluntarios/Mariel.jpg" alt="Avatar" style="width:80%">
                        <P>MARIEL FLORES DÍAZ</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <div>
        <H2 class="w3-center">Clan de Rovers</H2>
        <br>
        <div class="w3-row">
            <div class="w3-container w3-third">
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <h4>cONSEJERO ROVER RESPONSABLE</h4>
                        <br>
                        <img src="img/voluntarios/Alberto Aparicio.jpg" alt="Avatar" style="width:80%">
                        <P>ALBERTO APARICIO LAZCANO</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
            </div>
        </div>
        <br>
    </div>
    <div>
        <H2 class="w3-center">Colaboradores de grupo</H2>
        <br>
        <div class="w3-row">
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <br>
                        <img src="img/voluntarios/Liliana.jpg" alt="Avatar" style="width:80%">
                        <P>LILINA GARCIA CASTELAN</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <br>
                        <img src="img/voluntarios/avatar.png" alt="Avatar" style="width:80%">
                        <P>LUIS ALAM ISLAS RIVERA</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-container w3-third">
                <div class="w3-card-4">
                    <div class="w3-container w3-center">
                        <br>
                        <img src="img/voluntarios/avatar.png" alt="Avatar" style="width:80%">
                        <P>ROBERTO DE JESÚS ARTEGA MARTINEZ</P>
                        <div class="w3-section">
                            <button class="w3-button backOficial">Contacto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>