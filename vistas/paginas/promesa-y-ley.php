<div class="w3-container" style="padding: 0px 15%;">
    <h1 style="text-align: justify;">PROMESA Y LEY</h1>

    <img src="img/promesayley.png" style="width: 100%;" alt="promesa y ley">
    <h4>NUESTRA PROMESA</h4>
    <p>La promesa es la manifestación de una transformación interna, el que realiza la Promesa Scout entra en una nueva condición o etapa de su vida, es un acto libre, un compromiso personal e intransferible adquirido voluntariamente.
    </p>
    <h6 class="w3-center"><i>&quot; Yo prometo por mi honor hacer cuanto de mí dependa para cumplir mis deberes
            para
            con Dios y la Patria, ayudar al prójimo en toda circunstancia y cumplir fielmente la ley scout&quot;.</i></p>

        <h4>NUESTRA LEY</h4>
        <p>Nuestra ley es un decálogo que no prohibe nada y es un código de conducta que encausa las acciones. Esta ley es la misma en todos los grupos y asociaciones scouts.</p>
        <ol class="ley">
            <li>El Scout cifra su honor en ser digno de confianza.</li>
            <li>El Scout es leal con su patria, sus padres, sus jefes y subordinados.</li>
            <li>El Scout es útil y ayuda a los demás sin pensar en recompensa.</li>
            <li>El Scout es amigo de todos y hermano de todo Scout, sin distinción de credo, raza, nacionalidad o clase social.
            </li>
            <li>El Scout es cortés y actúa con nobleza.</li>
            <li>El Scout ve en la naturaleza la obra de Dios, protege a los animales y plantas.</li>
            <li>El Scout obedece con responsabilidad y hace las cosas en orden y completas.</li>
            <li>El Scout ríe y canta en sus dificultades.</li>
            <li>El Scout es económico, trabajador y cuidadoso del bien ajeno.</li>
            <li>El Scout es limpio, sano y puro de pensamientos, palabras y acciones.</li>
        </ol>
</div>