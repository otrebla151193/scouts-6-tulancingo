<div class="w3-container" style="padding: 0px 15%;">
    <h1 style="text-align: justify;">ORDENAMIENTOS y formatos</h1>

    <h4>Ordenamientos</h4>
    <ol>
        <li>declaración de principios</li>
        <li>estatutos</li>
        <li>reglamento</li>
        <li>Manual de operación nivel grupo</li>
    </ol>
    <h4>Programa</h4>
    <ol>
        <li>politica nacional de programa de jóvenes</li>
        <li>politica nacional de participación juvenil</li>
        <li>carnet de vida al aire libre</li>
        <li>ids</li>
        <li>Programas mundiales</li>
        <li>competencias</li>
        <li>especialidades</li>
    </ol>
    <h4>registro</h4>
    <ol>
        <li>código de conducta</li>
        <li>autorización de ingreso al movimiento para menores de edad</li>
        <li>solicitud de ingreso reingreso y continuidad para mayores de edad a la asociación de scouts de méxico</li>
    </ol>
    <h4>formatos</h4>
    <ol>
        <li>ciclo de programa</li>
        <li>progresión personal de todas las secciones</li>
        <li>programa de actividades</li>
        <li>solicitud de actividad fuera del local</li>
        <li>Protocolo de seguridad en el transporte</li>
        <li>PLan de contingencia</li>
        <li>IDOS</li>
        <li>Programas mundiales</li>
    </ol>
    <h4>GESTIÓN DE RIESGOS</h4>
</div>