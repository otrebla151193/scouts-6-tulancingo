<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0&appId=1748763862018087&autoLogAppEvents=1"></script>
<div class="w3-container" style="padding: 0px 15%;">
    <h1 style="text-align: justify;">ÚNETE</h1>

    <div class="w3-row">
        <div class="w3-half w3-container w3-cell-middle w3-cell">
            <img src="img/unete/informacion.png" class="w3-round w3-image " alt="Photo of Me" width="500" height="333">
        </div>

        <!-- Hide this text on small devices -->
        <div class="w3-half w3-container w3-justify w3-cell-middle w3-cell">
            <p>Somos parte del movimiento juvenil más grande del mundo, cuya misión es contribuir a la
                educación de niños y jóvenes, a través de un sistema de valores basados en la Promesa y Ley Scout, generando un
                mundo mejor.</p>
            <p>
                Integrate a nuestro grupo, comprometido desde 1978 a potenciar tus habilidades, desde los
                siete años de edad y hasta los que quieras...
            </p>
        </div>
    </div>
    <br>
    <h4>¿Cómo ingresar al grupo?</h4>
    <p class="w3-justify">Ser Scout es una oportunidad única para convertirte en una persona diferente, creativa, autosuficiente y servicial, todo esto mientras te diviertes, aprendes cosas nuevas y formas parte de la organización juvenil más importante del mundo. Acepta el reto, intégrate y únete. Los pasos para formar parte de nuestro movimiento son:</p>
    <ol>
        <li>Llena el siguiente formulario para que podamos ponernos en contacto lo más pronto posible.</li>
        <div class="">
            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdbBePnW9HNDNGLDTDSaefOJWlgdvfQt-lsL3ohlo_CPLvVHg/viewform?embedded=true" width="100%" height="500" frameborder="0" marginheight="0" marginwidth="0" id="formularioRegistro">Cargando…</iframe>
        </div>
        <li>
            Acude a nuestro local de grupo donde nos reunimos cada sábado:
            <ul>
                <li>Si tienes de 7 a 10 años acude al centro de la ciudad donde está el monumento a Benito Juárez</li>
                <li>Si tienes más de 10 años acude a un costado del PAMAR (Cancha de futbol a un costado del parque recreativo "El Caracol")</li>
                <div class="w3-center">
                    <iframe src="https://www.google.com/maps/d/u/1/embed?mid=1scxPJ_1AL-UjJ7XY-PR3A6xnwXaRmifS" width="90%" height="480"></iframe>
                </div>
            </ul>
        </li>
        <li>Pregunta a cualquier Scouter (Jefes de sección), administradora de grupo o demás colaboradores quienes te darán toda la información necesaria para ingresar.</li>
        <li>Ese día, intégrate a la Sección correspondiente. Esta fase es muy importante para que empieces a hacer amigos y conozcas como trabajamos. <b>Ven 3 sábados de prueba sin compromiso.</b> </li>
        <li>Si te gustó, entonces estás list@ para ser scout.</li>
        <li>Descarga los formatos que están disponibles en el siguiente <a href="">link</a>, llénalos con apoyo de tus padres o tutores y entregalos al scouter de la sección o alguno de los colaboradores de grupo, o bien, puedes pedirles a ellos los formatos.</li>
        <li>Una vez nos hayas entregado los formatos, la administración del grupo realizará los tramites para que puedas realizar tu pago a la Asociación de Scouts de México AC.</li>
        <li>Solo falta un paso más, tendrás que adquirir tu uniforme y esto puedes hacerlo con los colaboradores del grupo o directamente en la <a href="https://scouts.org.mx/tiendascout-2/" target="_blank">tienda scout nacional.</a></li>
    </ol>
    <p><b>Ahora si, ya estás list@ para vivir la más grande aventura de tu vida. ¡Ser un scout!</b></p>

    <h4 class="w3-center">sÍGUENOS EN NUESTAR REDES SOCIALES</h4>
    <br>

    <div class="w3-container w3-center">
        <div class="fb-page" data-href="https://www.facebook.com/Grupo6Quetzalcoatl/" data-tabs="timeline" data-width="550" data-height="800" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/Grupo6Quetzalcoatl/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Grupo6Quetzalcoatl/">Scouts 6 Tulancingo</a></blockquote>
        </div>
    </div>
</div>
<br>
<br>