<div class="w3-container" style="padding: 0px 15%;">
    <h1 style="text-align: justify;">MISIÓN Y VISIÓN</h1>

    <img src="img/pronto.png" style="width: 100%; height: 500px" alt="">

    <h4>NUESTRA MISIÓN</h4>
    <p class="w3-justify">La Misión del Movimiento Scout es contribuir a la educación de los jóvenes, mediante un sistema de valores basado en la Promesa y la Ley Scout, para ayudar a construir un mundo mejor donde las personas se sientan realizadas como individuos y jueguen
        un papel constructivo en la sociedad.</p>

    <h4>NUESTRA VISIÓN</h4>
    <p class="w3-justify">En congruencia con la Visión de la Organización Mundial del Movimiento Scout, la visión específica de la Asociación de Scouts de México, A.C., es que para 2023 los Scouts serán el movimiento juvenil más grande del país, permitiendo que más de cien
        mil jóvenes sean Ciudadanos activos, creando un cambio positivo en sus comunidades y en el mundo con base en valores compartidos.</p>

    <h4>VISIÓN MUNDIAL</h4>
    <p class="w3-justify">Para 2023, Los Scouts serán el movimiento juvenil educativo líder a nivel mundial, permitiendo que 100 millones de jóvenes sean ciudadanos activos creando un cambio positivo en sus comunidades y en el mundo con base en valores compartidos.</p>
</div>